import React from 'react'
import { List } from 'antd';
import ItemMovie from './ItemMovie';

function ListMovie(props) {
    return (
        <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={props.items}
            renderItem={item => (
                <List.Item>
                    <ItemMovie item={item} onItemMovieClick={props.onItemMovieClick}/>
                </List.Item>
            )}
        />
    )
}

export default ListMovie
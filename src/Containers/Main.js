import React, { Component } from 'react';
import { Spin, Modal, Button, Layout, Menu } from 'antd';
import RouteMenu from './RouteMenu';


const { Header, Content, Footer } = Layout;
const menus = ['movies', 'favorite', 'profile'];

class Main extends Component {

    state = {
        items: [],
        isShowModal: false,
        itemMovie: null,
        pathName: menus[0]
    };

    onItemMovieClick = (item) => {
        this.setState({ isShowModal: true, itemMovie: item })
    }
    onModalClickOk = () => {
        this.setState({ isShowModal: false })
    }
    onModalClickCancel = () => {
        this.setState({ isShowModal: false })
    }


    componentDidMount() {
        const { pathname } = this.props.location;
        var pathName = menus[0];
        if (pathname != '/'){
            pathName = pathname.replace('/','');
            if(!menus.includes(pathName)) pathName = menus[0];
        }
        this.setState({ pathName });
        fetch('http://workshopup.herokuapp.com/movie')
            .then(response => response.json())
            .then(movies => this.setState({ items: movies.results }))

    }

    onMenuClick = e => {
        var path = '/';
        if (e.key != 'home') {
            path = `/${e.key}`;
        }
        this.props.history.replace(path);
    };

    render() {
        const item = this.state.itemMovie;
        return (
            <div>
                {this.state.items.length > 0 ? (
                    <div style={{ height: '100vh' }}>
                        <Layout className="layput" style={{ background: "orange" }}>
                            <Header
                                style={{
                                    padding: '0px',
                                    position: 'fixed',
                                    zIndex: 1,
                                    width: '100%'
                                }}
                            >
                                <Menu
                                    theme="dark"
                                    mode="horizontal"
                                    defaultSelectedKeys={[this.state.pathName]}
                                    style={{ lineHeight: '64px' }}
                                    onClick={e => {
                                        this.onMenuClick(e);
                                    }}
                                >
                                    <Menu.Item key={menus[0]}>Home</Menu.Item>
                                    <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                                    <Menu.Item key={menus[2]}>Profile</Menu.Item>
                                </Menu>
                            </Header>
                            <Content
                                style={{
                                    padding: '16px',
                                    marginTop: 64,
                                    minHeight: '600px',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    display: 'flex'
                                }}
                            >
                                <RouteMenu
                                    items={this.state.items}
                                    inItemMovieClick={this.onItemMovieClick}
                                />
                            </Content>
                            <Footer style={{ textAlign: 'center', background: '#17202a', color: 'white'}}>
                                Movie Application Workshop @ CAMT
                            </Footer>
                        </Layout>
                    </div>

                ) : (
                        <Spin size="large" />
                    )}
                {item != null ? (
                    <Modal
                        width="40%"
                        style={{ maxHeight: '70%' }}
                        title={this.state.itemMovie.title}
                        visible={this.state.isShowModal}
                        onCancel={this.onModalClickCancel}
                        footer={[
                            <Button
                                key="Submit"
                                type="primary"
                                icon="heart"
                                size="large"
                                shape="circle"
                                onClick={this.handleFavorite}
                            />,
                            <Button
                                key="Submit"
                                type="primary"
                                icon="shopping-cart"
                                size="large"
                                shape="circle"
                                onClick={this.onClickBuyTicket}
                            />
                        ]}
                    >
                        <img src={item.image_url} style={{ width: '100%' }} />
                        <br />
                        <br />
                        <p>{item.overview}</p>
                    </Modal>
                ) : (
                        <div />
                    )}
            </div>
        );
    }
}

export default Main